module botimage

go 1.14

require (
	github.com/fogleman/gg v1.3.0
	github.com/ghiac/bimg v1.0.19
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/gorilla/mux v1.7.4
	github.com/h2non/bimg v1.1.4 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	golang.org/x/image v0.0.0-20200801110659-972c09e46d76 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
)
