package api

import "botimage/api/controllers"

var server = controllers.Server{}
func Run()  {
	server.Initialize()
	server.Run(":8080")
}