package controllers

import (
	"botimage/api/responses"
	"encoding/json"
	"github.com/fogleman/gg"
	"log"
	"math/rand"
	"net/http"
	"path/filepath"
	"strings"
	"time"
)

type FlightData struct {
	StartDate string `json:"start_date"`
	EndDate string `json:"end_date"`
	StartDateR string `json:"start_date_r"`
	EndDateR string `json:"end_date_r"`
	Departure string `json:"departure"`
	Destination string `json:"destination"`
	TypeTrip string `json:"typetrip"`
}

func ImageLabel(start_date string, start_date_r string, end_date string, end_date_r string, departure string, destination string, typetrip string, filename string)  {

	dir, err := filepath.Abs(".")
	im, err := gg.LoadImage(dir+"/api/controllers/zyn.png")
	if err != nil {
		log.Fatal(err)
	}
	dc := gg.NewContext(1200,628)
	dc.SetRGB(1,1,1)
	dc.Clear()
	dc.SetRGB(0,0,0)
	dc.DrawImage(im, 0, 0)

	if err := dc.LoadFontFace(dir+"/fonts/roboto/Roboto-Regular.ttf", 52.2); err != nil {
		panic(err)
	}
	dc.DrawStringAnchored(departure, 379, 280, 0.5, 0.5)
	dc.DrawStringAnchored(destination, 376, 529, 0.5, 0.5)

	dc.DrawStringAnchored(destination, 1026, 280, 0.5, 0.5)
	dc.DrawStringAnchored(departure, 1025, 529, 0.5, 0.5)

	if err := dc.LoadFontFace(dir+"/fonts/Berlin/Berlin_Sans_FB_Regular.ttf", 76.9); err != nil {
		panic(err)
	}
	dc.DrawStringAnchored(start_date, 386, 204, 0.5, 0.5)
	dc.DrawStringAnchored(start_date_r, 386, 453, 0.5, 0.5)

	dc.DrawStringAnchored(end_date, 1029, 204, 0.5, 0.5)
	dc.DrawStringAnchored(end_date_r, 1030, 453, 0.5, 0.5)

	dc.SavePNG("./media/"+filename+".png")
}

func (s *Server) ImageManupulation(w http.ResponseWriter, r *http.Request)  {
	// Unique file name generator
	rand.Seed(time.Now().Unix())
	var output strings.Builder
	charSet := []rune("ab8cde7dfg3hi2jklmn6opqr3stABCDE7F9GHI0JKLMNOP£")
	length := 20
	for i := 0; i < length; i++ {
		random := rand.Intn(len(charSet))
		randomChar := charSet[random]
		output.WriteRune(randomChar)
	}
	filename := output.String()
	// Unique file name generator
	decoder := json.NewDecoder(r.Body)
	var data FlightData
	err := decoder.Decode(&data)
	if err != nil {
		panic(err)
	}


	go ImageLabel(data.StartDate,data.StartDateR, data.EndDate, data.EndDateR, data.Departure, data.Destination, data.TypeTrip, filename)
	responses.JSON(w, http.StatusOK, "http://localhost:8080/media/"+filename+".png")
}