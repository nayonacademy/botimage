package controllers

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"log"
	"net/http"
)

type Server struct {
	DB *gorm.DB
	Router *mux.Router
}

func (s *Server) Initialize() {
	s.Router = mux.NewRouter()
	s.InitialRoutes()
}

func(s *Server) Run(add string){
	fmt.Println("Listen to the port", add)
	log.Fatal(http.ListenAndServe(add, s.Router))
}