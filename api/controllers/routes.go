package controllers

import (
	"net/http"
)

func (s *Server) InitialRoutes(){
	s.Router.HandleFunc("/", s.Home).Methods("GET")
	s.Router.HandleFunc("/image", s.ImageManupulation).Methods("POST")
	s.Router.PathPrefix("/media/").Handler(http.StripPrefix("/media/", http.FileServer(http.Dir("./media"))))
}